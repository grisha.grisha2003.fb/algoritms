﻿#include <iostream>
#include <vector>
#include <random>

void ShellSort(std::vector<int> &mas)
{
	int n = mas.size();
	int d = n / 2;
	int tmp;
	while (d > 0)
	{
		for (int i = 0; i < n - d; i++)
		{
			int j = i;
			while (j >= 0 && mas[j] > mas[j + d])
			{
				tmp = mas[j];
				mas[j] = mas[j + d];
				mas[j + d] = tmp;
				j--;
			}
		}
		d = d / 2;
	}
};

int main()			
{
	std::vector<int> mas;
	int n = rand() % 100;
	for (int i = 0; i < n; i++)
		mas.push_back(rand() % 100);

	ShellSort(mas);
	
	for (int i = 0; i < n; i++)
		std::cout << mas[i] << ' ';


	return 0;
}