﻿#include <iostream>
#include <string>
#include <stack>

int main()
{
	setlocale(LC_ALL, "Rus");
	std::string str;
	std::cin >> str;
	std::stack<char> stack;

	bool noMorePop = false;
	for (int i = 0; i < int(str.size()); i++)
	{
		switch (str[i])
		{
		case '(':
			stack.push(str[i]);
			break;
		case '{':
			stack.push(str[i]);
			break;
		case '[':
			stack.push(str[i]);
			break;
		case ')':
			if (stack.size() == 0)
			{
				noMorePop = true;
				break;
			}
			if (stack.top() == '(')
				stack.pop();
			break;
		case '}':
			if (stack.size() == 0)
			{
				noMorePop = true;
				break;
			}
			if (stack.top() == '{')
				stack.pop();
			break;
		case ']':
			if (stack.size() == 0)
			{
				noMorePop = true;
				break;
			}
			if (stack.top() == '[')
				stack.pop();
			break;
		default:
			std::cout << "Введите строку только из скобок" << std::endl;
			break;
		}
	}

	if (stack.size() == 0)
		std::cout << std::endl << "Строка существует";
	else
		std::cout << std::endl << "Строка не существует";

	return 0;
}