﻿#include <iostream>
#include <vector>
#include <random>


void QuickSort(std::vector <int> &mas, int left, int right) 
{
    int i = left, j = right;
    int tmp;
    int pivot = mas[(left + right) / 2];

    while (i <= j) {
        while (mas[i] < pivot)
            i++;
        while (mas[j] > pivot)
            j--;
        if (i <= j) {
            tmp = mas[i];
            mas[i] = mas[j];
            mas[j] = tmp;
            i++;
            j--;
        }
    };

    if (left < j)
        QuickSort(mas, left, j);
    if (i < right)
        QuickSort(mas, i, right);

};


int main()
{
    std::vector<int> mas;
    int n = rand() % 100;
    for (int i = 0; i < n; i++)
        mas.push_back(rand() % 100);

    QuickSort(mas, 0 , mas.size()-1);

    for (int i = 0; i < n; i++)
        std::cout << mas[i] << ' ';


    return 0;
}
// Худшее n^2
// Среднее и лучшее  n log2 n