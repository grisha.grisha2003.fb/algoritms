﻿#include <iostream>
#include <vector>
#include <random>

void InsertSort(std::vector<int>& mas)
{
	int n = mas.size() - 1;

	for (int i = 1; i < n; i++)
		for (int j = i; j > 0 && mas[j - 1] > mas[j]; j--)
			std::swap(mas[j - 1], mas[j]);
}; //лучший O(n), средний и худший O(n^2)

int main()
{
	std::vector<int> mas;
	int n = rand() % 100;
	for (int i = 0; i < n; i++)
		mas.push_back(rand() % 100);

	InsertSort(mas);

	for (int i = 0; i < n; i++)
		std::cout << mas[i] << ' ';


	return 0;
}
