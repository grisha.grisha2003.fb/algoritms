﻿#include <iostream>
#include <vector>
#include <random>

void CombSort(std::vector<int>& mas)
{
	double factor = 1.2473309; // фактор уменьшения  = 1/(1-e^(-f)) 
	int step = mas.size() - 1;


	while (step >= 1)
	{
		for (int i = 0; i + step < mas.size(); i++)
		{
			if (mas[i] > mas[i + step])
			{
				std::swap(mas[i], mas[i + step]);
			}
		}
		step /= factor;
	}
};

int main()			
{
	std::vector<int> mas;
	int n = rand() % 100;
	for (int i = 0; i < n; i++)
		mas.push_back(rand() % 100);

	CombSort(mas);
	
	for (int i = 0; i < n; i++)
		std::cout << mas[i] << ' ';


	return 0;
}