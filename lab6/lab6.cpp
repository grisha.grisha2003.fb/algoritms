﻿#include <vector>
#include <random>
#include <iostream>

void SelectionSort(std::vector<int> &mas)
{
    for (int i = 0; i < mas.size() - 1; i++)
    {
        int min_index = i;
        for (int j = i + 1; j < mas.size(); j++)
        {
            if (mas[j] < mas[min_index])
            {
                min_index = j;
            }
        }
        if (min_index != i)
        {
            std::swap(mas[i], mas[min_index]);
        }
    }
}
int main()
{
	std::vector<int> mas;
	int n = rand() % 100;
	for (int i = 0; i < n; i++)
		mas.push_back(rand() % 100);

	SelectionSort(mas);

	for (int i = 0; i < n; i++)
		std::cout << mas[i] << ' ';


	return 0;
} // Худшее, среднее и лушчее время - n^2
